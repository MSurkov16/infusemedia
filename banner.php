<?php
/** Include main constants */
include_once './config.php';

/** Check main function */
if (!file_exists(COMPOSER))
    die(file_get_contents(IMG_ERROR_COMPOSER));
require_once COMPOSER;

try {
    $db = new Database\Connect();
    $db->checkTables(['history']);
    $obUser = new User\Information();
    \Database\History::saveToHistory($obUser->getData('REMOTE_ADDR'), $obUser->getData('HTTP_USER_AGENT'), $obUser->getData('HTTP_REFERER'));
} catch (\Database\Exception $e) {
    $ob = new Draw\Error();
    $ob->getImage("{$e->getMessage()}. Code: {$e->getCode()}");
} catch (Exception $e) {
    $ob = new Draw\Error();
    $ob->getImage("{$e->getMessage()}. Code: {$e->getCode()}");
}

echo file_get_contents(IMG_SUCCESS);