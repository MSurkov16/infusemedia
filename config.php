<?php

const COMPOSER = __DIR__.'/vendor/autoload.php';
const DB_HOST = 'localhost';
const DB_USER = 'root';
const DB_PASSWORD = '';
const DB_DATABASE = 'test';
const DB_TYPE = 'mysqli'; // only mysqli

const IMG_ERROR_COMPOSER = './img/composer.png';
const IMG_SUCCESS = './img/space.jpg';

