<?php


namespace Database;

/**
 * Class Connect
 * @package Database
 */
class Connect
{
    protected $connect = null;
    protected $className = '';

    /**
     * Connect constructor.
     */
    public function __construct()
    {
        $this->connect = new Provider(DB_TYPE);
        $this->className = $this->connect->className;
    }

    /**
     * @return mixed
     */
    public static function getConnect()
    {
        $ob = new self();
        return $ob->className::getInstance()->getConnection();
    }

    /**
     * @return string|null
     */
    public function getClassName() : string
    {
        return $this->className;
    }

    /**
     * @param array $tables
     */
    public function checkTables(array $tables)
    {
        $this->className::checkTables($tables);
    }
}