<?php

namespace Database;

/**
 * Class Exception
 * @package Database
 */
class Exception extends \Exception
{}