<?php

namespace Database;

/**
 * Class History
 * @package Database
 */
class History
{

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function getNameTable() : string
    {
        return strtolower((new \ReflectionClass($this))->getShortName());
    }

    protected function checkValues(string $ip) : int
    {
        $resIp = ip2long($ip);
        if ($resIp === false) throw new ExceptionAction("Error ip {$ip}", 1);
        return $resIp;
    }

    /**
     * @param string $ip
     * @param string $agent
     * @param string $page
     * @throws Exception
     * @throws \ReflectionException
     */
    public static function saveToHistory(string $ip, string $agent, string $page): void
    {
        try {
            $DB = Connect::getConnect();
            $ob = new self();
            $ip = $ob->checkValues($ip);
            $text_query = 'SELECT * FROM '.$ob->getNameTable().' WHERE';
            $text_query .= " `ip_address` = %u";
            $text_query .= " AND `user_agent` = '%s'";
            $text_query .= " AND `page_url` = '%s'";
            $text_query .= ";";
            $query = sprintf($text_query,
                $DB->real_escape_string($ip),
                $DB->real_escape_string($agent),
                $DB->real_escape_string($page),
            );
            $result = $DB->query($query);
            if ($result->num_rows === 0) {
                $text_query = 'INSERT INTO '.$ob->getNameTable()." (`ip_address`, `user_agent`, `page_url`, `views_count`, `view_date`) VALUES ( %u, '%s', '%s', 1, FROM_UNIXTIME(%u));";
                $query = sprintf($text_query,
                    $DB->real_escape_string($ip),
                    $DB->real_escape_string($agent),
                    $DB->real_escape_string($page),
                    time()
                );
                if(!$DB->query($query))
                    throw new Exception("Connect to Mysql has error <b>{$DB->error}</b>", $DB->errno);
            } else {
                $params = $result->fetch_assoc();

                $text_query = 'UPDATE '.$ob->getNameTable()." SET view_date = FROM_UNIXTIME(%u), views_count = `views_count` + 1 WHERE";
                $text_query .= " `ip_address` = {$params['ip_address']}";
                $text_query .= " AND `user_agent` = '{$params['user_agent']}'";
                $text_query .= " AND `page_url` = '{$params['page_url']}'";
                $text_query .= ";";

                $query = sprintf($text_query,
                    time()
                );

                if(!$DB->query($query)) {
                    throw new Exception("Connect to Mysql has error <b>{$DB->error}</b>", $DB->errno);
                }
            }

        } catch (\Database\ExceptionAction $e) {
            throw new Exception("{$e->getMessage()}", $e->getCode());
        } catch (Exception $e) {
            throw new Exception("{$e->getMessage()}", $e->getCode());
        }

    }
}