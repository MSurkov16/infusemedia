<?php

namespace Database;


/**
 * Class Mysqli is Singleton
 * @package Database
 */
class Mysqli {

    private $_connection;
    private static $_instance;

    /**
     * @return static
     */
    public static function getInstance(): Mysqli
    {
        if(!self::$_instance)
            self::$_instance = new static();
        return self::$_instance;
    }

    /**
     * Mysqli constructor.
     * @throws Exception
     */
    private function __construct()
    {
        $this->_connection = @new \mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
        if($this->_connection->connect_errno)
            throw new Exception("Connect to Mysql has error <b>{$this->_connection->connect_error}</b>", $this->_connection->connect_errno);
    }


    /**
     * can't if singleton
     */
    function __clone() {}

    /**
     * can't if singleton
     */
    function __wakeup(){}

    /**
     * @return \mysqli
     */
    public function getConnection(): \mysqli
    {
        return $this->_connection;
    }

    /**
     * @param array $tables
     * @throws Exception
     */
    public static function checkTables(array $tables) :void
    {

        $DB = self::getInstance()->getConnection();
        $text_query = 'SHOW TABLES LIKE';
        for ($i = 0; $i < count($tables); $i++) {
            if ($i > 0)
                $text_query .= ' OR LIKE';
            $text_query .= " '%s'";
        }

        $query = vsprintf($text_query, $tables);

        $result = $DB->query($query);
        if ($result->num_rows != count($tables))
            throw new Exception("Some tables were not found (".implode($tables, ',')."). Please create their", 1);
    }
}