<?php

namespace Database;


/**
 * Class Provider
 * @package Database
 */
class Provider
{
    const CONNECTION_TYPES = [
        'mysqli'
    ];

    public $className = '';

    /**
     * Provider constructor.
     * @param string $typeConnect
     * @throws ExceptionProvider
     */
    public function __construct(string $typeConnect)
    {
        if (!in_array($typeConnect, self::CONNECTION_TYPES))
            throw new ExceptionProvider("Connection type not found",1);

        $this->className = "\Database\\".ucfirst($typeConnect);

        if (!class_exists($this->className))
            throw new ExceptionProvider("Connection class not found",2);

        $this->className::getInstance();
        return $this->className;
    }
}