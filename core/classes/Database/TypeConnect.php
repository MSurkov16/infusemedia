<?php


namespace Database;

/**
 * Interface TypeConnect
 * @package Database
 */
interface TypeConnect
{
    /**
     * @return mixed
     */
    static function getInstance();

    /**
     * @param array $checkTables
     * @return mixed
     */
    static function checkTables(array $checkTables);
}