<?php


namespace Draw;

/**
 * Class Error
 * @package Draw
 */
class Error
{
    protected $font;
    protected $color_text = [
        0, 0, 0
    ];
    protected $color_background = [
        255, 255, 255
    ];
    protected $width = 900;
    protected $height = 70;

    /**
     * Error constructor.
     * @param string $font_path
     */
    public function __construct(string $font_path = '')
    {
        if (empty($font_path))
            $this->font = $_SERVER['DOCUMENT_ROOT'] . '/core/fonts/arial.ttf';
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width): void
    {
        $this->width = $width;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height): void
    {
        $this->height = $height;
    }

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     */
    public function setColorText(int $red, int $green, int $blue): void
    {
        $this->color_text = [$red, $green, $blue];
    }

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     */
    public function setColorBackground(int $red, int $green, int $blue): void
    {
        $this->color_background = [$red, $green, $blue];
    }

    /**
     * @param string $text
     */
    public function getImage(string $text = '')
    {
        header('content-type: image/png');
        $image = imagecreate((strlen($text)*6.06), $this->height);

        imagecolorallocate($image, array_shift($this->color_background), array_shift($this->color_background), array_shift($this->color_background));
        $white = imagecolorallocate($image, array_shift($this->color_text), array_shift($this->color_text), array_shift($this->color_text));
        imagettftext($image, 10, 0, 0, 30, $white, $this->font, $text);

        $file = $_SERVER['DOCUMENT_ROOT'].'/core/img/'.rand(1,10000).'.png';
        imagepng($image, $file);
        imagedestroy($image);
        echo file_get_contents($file);
        die();
    }
}