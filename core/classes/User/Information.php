<?php


namespace User;

/**
 * Class Information
 * @package User
 */
class Information
{
    protected $data = null;

    /**
     * Information constructor.
     */
    public function __construct() {
        $this->data = $_SERVER;;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getData(string $key)
    {
        return $this->data[$key];
    }
}