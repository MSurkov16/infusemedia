CREATE TABLE `history` (
	`ip_address` INT(11) NOT NULL DEFAULT '0',
	`user_agent` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8mb4_unicode_ci',
	`view_date` TIMESTAMP NOT NULL,
	`page_url` LONGTEXT NULL COLLATE 'utf8mb4_unicode_ci',
	`views_count` INT(11) NOT NULL DEFAULT '0'
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB
;
